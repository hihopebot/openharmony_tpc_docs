### **isEmpty**
>+ openharmony API: ohos.agp.utils.TextTool.isNullOrEmpty
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
String str = "";
// 判断是否为空或者为null
TextTool.isNullOrEmpty(str);

```

### **equals**
>+ openharmony API: ohos.agp.utils.TextTool.isEqual
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
String strA = "TestA";
String strB = "TestB";
// 判断是否相等
TextTool.isEqual(strA, strB);

```

