### **closeDrawer**
>+ openharmony API: ohos.agp.components.SlideDrawer.close(closeSmoothly)
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
// 不带动画直接关闭
slideDrawer.close();

// 不带动画向指定方向关闭
slideDrawer.close(SlideDrawer.SlideDirection.START);

// 带动画效果的关闭方式
slideDrawer.closeSmoothly();

// 带动画(指定动画时间)效果的关闭方式
slideDrawer.closeSmoothly(350);

// 带动画向指定方向关闭
slideDrawer.closeSmoothly(SlideDrawer.SlideDirection.START);

// 带动画(指定动画时间)向指定方向关闭
slideDrawer.closeSmoothly(SlideDrawer.SlideDirection.START, 350);
```

### **openDrawer**
>+ openharmony API: ohos.agp.components.SlideDrawer.open(openSmoothly)
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
// 不带动画直接打开
slideDrawer.open();

// 不带动画向指定方向打开
slideDrawer.open(SlideDrawer.SlideDirection.START);

// 带动画效果的打开方式
slideDrawer.openSmoothly();

// 带动画(指定动画时间)效果的打开方式
slideDrawer.openSmoothly(350);

// 带动画向指定方向打开
slideDrawer.openSmoothly(SlideDrawer.SlideDirection.START);

// 带动画(指定动画时间)向指定方向打开
slideDrawer.openSmoothly(SlideDrawer.SlideDirection.START, 350);
```


