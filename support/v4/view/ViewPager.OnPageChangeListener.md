﻿### **onPageScrolled**
+ openharmony API: `ohos.agp.components.PageSlider.PageChangedListener.onPageSliding(int position, float positionOffset, int positionOffsetPixels)`
+ openharmony SDK版本：2.1.0.17
+ IDE版本：2.1.0.141
+ 实现方案：采用上面的API，但是当滑动到终点和往左滑动时，参数不一样，需要修正
  + 1.定义两个变量：
``` java
	private float downTouchX = 0;
	private boolean isScrollBack = false;
```
  + 2.在PageSlider的触摸事件中做处理，用来判断用户是否是往左滑动：
``` java
	@Override
   public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
   		switch (touchEvent.getAction()) {
      		case TouchEvent.PRIMARY_POINT_DOWN:
         		downTouchX = touchEvent.getPointerScreenPosition(0).getX();
         		break;
         case TouchEvent.POINT_MOVE:
         		float newTouchX = touchEvent.getPointerScreenPosition(0).getX();
         		isScrollBack = newTouchX > downTouchX;
         		break;
         default:
         		break;
    	}
    	return false;
    }
```
  + 3.在onPageSliding监听中先对值进行修正，然后再做要做的逻辑：
``` java
	@Override
   public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
		/**
		 * position:当前View的位置
		 * positionOffset:当前View的偏移量比例.[0,1)
		 */
		positionOffset = positionOffset == 1 ? 0 : positionOffset;
		if (isScrollBack) {
			position = position - 1;
			positionOffset = 1 - positionOffset;
		} else {
			if (position == pageSlider.getProvider().getCount() - 1 && positionOffset > 0.99) {
				position = 0;
				positionOffset = 0;
			}
		}
		if (positionOffset == 0) {
			isScrollBack = false;
		}
	//do yourself
	}
```