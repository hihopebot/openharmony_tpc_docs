﻿### **ofFloat，ofInt，setEvaluator**
## 方案一：
>+ openharmony class: ohos.agp.animation.Animator
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：

**对于ValueAnimator ofFloat(float... values)处理**
**原三方库:**
```java
final ValueAnimator valueAnimator = ValueAnimator.ofFloat(values);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
            }
        });
```
**openharmony :**
```java
final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float value = AnimatorValueUtils.getAnimatedValue(v,values);
            }
        });

/**
     * get the animated value with fraction and values
     *
     * @param fraction 0~1
     * @param values float array
     * @return float animated value
     */
public static float getAnimatedValue(float fraction, float... values){
        if(values == null || values.length == 0){
            return 0;
        }
        if(values.length == 1){
            return values[0] * fraction;
        }else{
            if(fraction == 1){
                return values[values.length-1];
            }
            float oneFraction = 1f / (values.length - 1);
            float offFraction = 0;
            for (int i = 0; i < values.length - 1; i++) {
                if (offFraction + oneFraction >= fraction) {
                    return values[i] + (fraction - offFraction) * (values.length - 1) * (values[i + 1] - values[i]);
                }
                offFraction += oneFraction;
            }
        }
        return 0;
    }
```

**对于ValueAnimator ofInt(int... values) + setEvaluator(new ArgbEvaluator())处理**
**原三方库:**
```java
 final ValueAnimator valueAnimator = ValueAnimator.ofInt(color));
        valueAnimator.setEvaluator(new ArgbEvaluator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int color = (int) valueAnimator.getAnimatedValue());
            }
        });
```
**openharmony :**
```java
final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                int color = getAnimatedColor(v,color);
            }
        });

 /**
     * get the animated value with fraction and values
     *
     * @param fraction 0~1
     * @param colors color array
     * @return int color
     */
public static int getAnimatedColor(float fraction, int... colors){
        if(colors == null || colors.length == 0){
            return 0;
        }
        if(colors.length == 1){
            return getAnimatedColor(0,colors[0],fraction);
        }else{
            if(fraction == 1){
                return colors[colors.length-1];
            }
            float oneFraction = 1f / (colors.length - 1);
            float offFraction = 0;
            for (int i = 0; i < colors.length - 1; i++) {
                if (offFraction + oneFraction >= fraction) {
                    return getAnimatedColor(colors[i], colors[i + 1], (fraction - offFraction) * (colors.length -1));
                }
                offFraction += oneFraction;
            }
        }
        return 0;
    }

    /**
     * get the animated color with start color, end color and fraction
     *
     * @param fraction 0~1
     * @param from start color
     * @param to end color
     * @return int color
     */
    public static int getAnimatedColor(int from, int to, float fraction) {
        RgbColor colorFrom = RgbColor.fromArgbInt(from);
        RgbColor colorTo = RgbColor.fromArgbInt(to);
        int red = (int) (colorFrom.getRed() + (colorTo.getRed() - colorFrom.getRed()) * fraction);
        int blue = (int) (colorFrom.getBlue() + (colorTo.getBlue() - colorFrom.getBlue()) * fraction);
        int green = (int) (colorFrom.getGreen() + (colorTo.getGreen() - colorFrom.getGreen()) * fraction);
        int alpha = (int) (colorFrom.getAlpha() + (colorTo.getAlpha() - colorFrom.getAlpha()) * fraction);
        RgbColor mCurrentColorRgb = new RgbColor(red, green, blue, alpha);
        return mCurrentColorRgb.asArgbInt();
    }
```

## 方案二：
>+ openharmony API: 无API替换
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：对于设置自定义`float`初始值和`float`结束值的数值动画，可使用以下自定义类，同理可写出其他自定义初始值和结束值的数值动画类：
``` java
		import ohos.agp.animation.AnimatorValue;

		/**
 		* 可以设置初始值和结束值的数值动画
 		*/
		public class MyValueAnimator extends AnimatorValue {

		    private float start = 0;
		    private float end = 1;
    		private ValueUpdateListener myValueUpdateListener;

    		/**
     		* 获取一个自定义初始值和结束值的数值动画对象
     		*
     		* @param start 起始值
     		* @param end   结束值
     		* @return 自定义初始值和结束值的数值动画对象
     		*/
    		public static MyValueAnimator ofFloat(float start, float end) {
        		MyValueAnimator myValueAnimator = new MyValueAnimator();
        		myValueAnimator.start = start;
        		myValueAnimator.end = end;
        		return myValueAnimator;
    		}

    		private MyValueAnimator() {
        		super.setValueUpdateListener(new ValueUpdateListener() {
            		@Override
            		public void onUpdate(AnimatorValue animatorValue, float value) {
                		value = value * (end - start) + start;
                		if (myValueUpdateListener != null) {
                    		myValueUpdateListener.onUpdate(animatorValue, value);
                		}
            		}
        		});
    		}

    		/**
     		* 设置数值动画的起始值和结束值
     		*
     		* @param start 起始值
     		* @param end   结束值
     		*/
    		public void setFloatValues(float start, float end) {
        		this.start = start;
        		this.end = end;
    		}

    		@Override
    		public void setValueUpdateListener(ValueUpdateListener listener) {
        		this.myValueUpdateListener = listener;
    		}

		}
```


### **setInterpolator**
>+ openharmony class: ohos.agp.animation.Animator
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：原三方库的ValueAnimator.setInterpolator(Interpolator interpolator)用ohos.agp.animation.AnimatorValue.setCurveType(int curveType)代替，其中curveType的int值是ohos.agp.animation.Animator.CurveType类的枚举常量.

### **setRepeatMode**
>+ openharmony API: 无API替换
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：
  + 1.定义两个状态常量值和一个Boolean值：
``` java
public final static int RESTART = 1;//正常模式
public final static int REVERSE = 2;//倒序回放模式
private int repeatMode = RESTART;
private boolean isBack = false;
```
  + 2.给数值动画设置重复执行时的监听，如下所示：
``` java
animatorValue.setLoopedListener(new Animator.LoopedListener() {

            @Override
            public void onRepeat(Animator animator) {
                isBack = !isBack;
            }
        });
```
  + 3.在数值动画的`ValueUpdate`监听中做如下处理：
``` java
animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                if (repeatMode == REVERSE && isBack) {
                    value = 1 - value;
                }
                //do yourself
            }
        });
```
### **ObjectAnimator ofFloat(Object target, String propertyName, float... values)(一种ohos替换方案)**
>+ openharmony class: ohos.agp.animation.AnimatorValue
>+ openharmony SDK版本：2.1.0.21
>+ IDE版本：2.1.0.220
>+ 实现方案：

**对于ObjectAnimator ofFloat(Object target, String propertyName, float... values)处理**
**原三方库:**
```java
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mTarget, "translationY", 0, dipToPixels(MyActivity.this, -(100)));
                objectAnimator.setDuration(2000);
                objectAnimator.start();
```
**openharmony :**
```java
                // 数值动画
                AnimatorValue animator = new AnimatorValue();
                animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        mTarget.setTranslationY( - vpToPixels(getContext(), 100) * v);
                    }
                });
                animator.setDuration(2000);
                animator.start();
```

### **ObjectAnimator ofFloat(Object target, String propertyName, float... values),setEvaluator(TypeEvaluator value)(一种ohos替换方案)**
>+ openharmony class: ohos.agp.animation.AnimatorValue
>+ openharmony SDK版本：2.1.0.21
>+ IDE版本：2.1.0.220
>+ 实现方案：

**对于ObjectAnimator ofFloat(Object target, String propertyName, float... values),setEvaluator(TypeEvaluator value)处理**
**原三方库:**

```java
 				Skill s = (Skill) view.getTag();
                AnimatorSet set = new AnimatorSet();
                mTarget.setTranslationX(0);
                mTarget.setTranslationY(0);
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mTarget, "translationY", 0, dipToPixels(MyActivity.this, -(157)));
                set.playTogether(
                        Glider.glide(s, 1200, objectAnimator, new BaseEasingMethod.EasingListener() {
                            @Override
                            public void on(float time, float value, float start, float end, float duration) {

                              mHistory.drawPoint(time, duration, value - dipToPixels(MyActivity.this, 60));
                            }
                        })
                );
                set.setDuration(1200);
                set.start();
                        
```

```java
public abstract class BaseEasingMethod implements TypeEvaluator<Number> {
    protected float mDuration;

    private ArrayList<EasingListener> mListeners = new ArrayList<EasingListener>();

    public interface EasingListener {
        public void on(float time, float value, float start, float end, float duration);
    }

    public void addEasingListener(EasingListener l){
        mListeners.add(l);
    }

    public void addEasingListeners(EasingListener ...ls){
        for(EasingListener l : ls){
            mListeners.add(l);
        }
    }

    public void removeEasingListener(EasingListener l){
        mListeners.remove(l);
    }

    public void clearEasingListeners(){
        mListeners.clear();
    }

    public BaseEasingMethod(float duration){
        mDuration = duration;
    }

    public void setDuration(float duration) {
        mDuration = duration;
    }

    @Override
    public final Float evaluate(float fraction, Number startValue, Number endValue){
        float t = mDuration * fraction;
        float b = startValue.floatValue();
        float c = endValue.floatValue() - startValue.floatValue();
        float d = mDuration;
        float result = calculate(t,b,c,d);
        for(EasingListener l : mListeners){
            l.on(t,result,b,c,d);
        }
        return result;
    }

    public abstract Float calculate(float t, float b, float c, float d);

}

                        
```

```java
public class Glider {

    public static ValueAnimator glide(Skill skill, float duration, ValueAnimator animator) {
        return Glider.glide(skill, duration, animator, (BaseEasingMethod.EasingListener[]) null);
    }

    public static ValueAnimator glide(Skill skill, float duration, ValueAnimator animator, BaseEasingMethod.EasingListener... listeners) {
        BaseEasingMethod t = skill.getMethod(duration);

        if (listeners != null)
            t.addEasingListeners(listeners);

        animator.setEvaluator(t);
        return animator;
    }

    public static PropertyValuesHolder glide(Skill skill, float duration, PropertyValuesHolder propertyValuesHolder) {
        propertyValuesHolder.setEvaluator(skill.getMethod(duration));
        return propertyValuesHolder;
    }


}
```
**openharmony :**
```java
public class MainAbility extends Ability {

    private Component mTarget;
    private long mDuration = 2000;

    private DrawView mHistory;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mTarget = findComponentById(ResourceTable.Id_target);
        mHistory = (DrawView) findComponentById(ResourceTable.Id_history);
        mHistory.clear();

        // 动画集合
        AnimatorGroup set = new AnimatorGroup();
        mTarget.setTranslationX(0);
        mTarget.setTranslationY(0);
        // 数值动画
        AnimatorValue animator = new AnimatorValue();
        animator.setDuration(mDuration);

        // 画最开始的点
        mHistory.drawPoint(0, mDuration, 0 - vpToPixels(getContext(), 60));
        set.runSerially(
                glide(  animator,new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float value) {

                        float start = 0;
                        float end = -vpToPixels(getContext(), 157) ;
                        float result = evaluate(value, start, end);
                        mTarget.setTranslationY(result);
                        mHistory.drawPoint(mDuration * value, mDuration, result - vpToPixels(getContext(), 60));
                    }
                }));
        set.setDuration(mDuration);
        set.start();

    }

    /**
     * vp转px
     *
     * @param context   上下文
     * @param vpValue vp数值
     * @return px
     */
    public static float vpToPixels(Context context, float vpValue) {
        return  AttrHelper.fp2px(vpValue,context);
    }

    public Float calculate(float t, float b, float c, float d) {
        if ((t/=d) < (1/2.75f)) {
            return c*(7.5625f*t*t) + b;
        } else if (t < (2/2.75f)) {
            return c*(7.5625f*(t-=(1.5f/2.75f))*t + .75f) + b;
        } else if (t < (2.5/2.75)) {
            return c*(7.5625f*(t-=(2.25f/2.75f))*t + .9375f) + b;
        } else {
            return c*(7.5625f*(t-=(2.625f/2.75f))*t + .984375f) + b;
        }
    }

    /**
     * 获得动画结果
     *
     * @param fraction 动画完成度
     * @param startValue 动画开始位置
     * @param endValue 动画结束位置
     * @return 动画结果
     */
    public final Float evaluate(float fraction, Float startValue, Float endValue){
        float t = mDuration * fraction;
        float b = startValue;
        float c = endValue- startValue;
        float d = mDuration;
        float result = calculate(t,b,c,d);
        return result;
    }

    public static AnimatorValue glide(

            AnimatorValue animator,
            AnimatorValue.ValueUpdateListener valueUpdateListener) {
        animator.setValueUpdateListener(valueUpdateListener);
        return animator;
    }
}

```