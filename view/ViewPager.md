
# PageSlider
* openharmony API: ohos.agp.components.PageSlider
* openharmony SDK版本：2.1.1.21
* IDE版本：2.1.0.501
* 实现方案:

openharmony：

``` java
  bannerView = (PageSlider)findComponentById(ResourceTable.Id_page_slider);
  bannerView.setProvider(new TabPagerProvider(this, titles))
```