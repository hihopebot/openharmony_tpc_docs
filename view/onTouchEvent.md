
# TouchEvent
* openharmony API: ohos.multimodalinput.event.TouchEvent
* openharmony SDK版本：2.1.1.21
* IDE版本：2.1.0.501
* 实现方案:

openharmony：

实现TouchEventListener接口，重写onTouchEvent方法
``` java
  public class Text extends Component implements Component.TouchEventListener{
  
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
          int action = touchEvent.getAction();
          MmiPoint point = touchEvent.getPointerPosition(touchEvent.getIndex());
          switch (action){
              case TouchEvent.PRIMARY_POINT_DOWN:
                  break;
              case TouchEvent.POINT_MOVE:
                  break;
              case TouchEvent.PRIMARY_POINT_UP:
                  break;
              case TouchEvent.NONE:
              case TouchEvent.CANCEL:
                  break;
              default:
                  break;
          }
          return false;
      }
```