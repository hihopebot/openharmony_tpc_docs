﻿### **addView**
>+ openharmony API: ohos.agp.window.dialog.PopupDialog.show 
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141 
>+ 实现方案：可以利用PopupDialog类，在特定场景中可以代替Android中WindowManager.addView()的效果。这里只是一种方案，并不是说明两者基于各自系统是同一类型的控件。下面是PopupDialog显示的代码片段，有更好的方法大家也可以分享。

```java
component = new Component(context);
component.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
component.addDrawTask(new Component.DrawTask() {
                    @Override
                    public void onDraw(Component component, Canvas canvas) {

                    }
                });
popupDialog = new PopupDialog(context, component,
                        context.getResourceManager().getDeviceCapability().screenDensity / 160 * context.getResourceManager().getDeviceCapability().width,
                        context.getResourceManager().getDeviceCapability().screenDensity / 160 * context.getResourceManager().getDeviceCapability().height
                );
popupDialog.setCustomComponent(component);
popupDialog.setTransparent(true);
popupDialog.setBackColor(new Color(0x00000000));
popupDialog.show();
```

>+ 补充说明：当前SDK版本：2.1.0.17不生效，后续支持。