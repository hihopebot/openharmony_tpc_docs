### **OnGlobalLayoutListener**
>+ openharmony API: ohos.agp.components.ComponentTreeObserver.WindowBoundListener
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：
使用addTreeLayoutChangedListener调用，监听没有回调，故使用addWindowBoundListener来进行替换，需要注意，该替换只会在首次Window添加Components后调用，不会像原库一样，Layout变化后，每次都会调用，需要自己具体情况自行考虑，手动触发监听。手动调用如下： ComponentTreeObserver.WindowBoundListener.onWindowBound();

```
componentView.getComponentTreeObserver().addWindowBoundListener(
    new ComponentTreeObserver.WindowBoundListener() {

        @Override
        public void onWindowBound() {
            componentView.this.layout.onWindowBound();
        }

        @Override
        public void onWindowUnbound() {
            try {
                componentView.getComponentTreeObserver().removeWindowBoundListener(this);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    });

```