### **UNSPECIFIED**
>+ openharmony API: ohos.agp.components.Component$EstimateSpec.UNCONSTRAINT
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：直接替换


### **EXACTLY**
>+ openharmony API: ohos.agp.components.Component$EstimateSpec.PRECISE
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：直接替换
    

### **AT_MOST**
>+ openharmony API: ohos.agp.components.Component$EstimateSpec.NOT_EXCEED
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：直接替换


### **MODE_MASK**
>+ openharmony API: ohos.agp.components.Component$EstimateSpec.ESTIMATED_STATE_BIT_MASK
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：直接替换


### **makeMeasureSpec**
>+ openharmony API: 暂无
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：暂无相关替换，可以自行实现。

```java
public static int makeEstimateSpec(int size, int mode) {
    return (size & ~EstimateSpec.ESTIMATED_STATE_BIT_MASK) | (mode & EstimateSpec.ESTIMATED_STATE_BIT_MASK);
}
```


### **getMode**
>+ openharmony API: ohos.agp.components.Component$EstimateSpec.getMode
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：直接替换


### **getSize**
>+ openharmony API: ohos.agp.components.Component$EstimateSpec.getSize
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：直接替换
