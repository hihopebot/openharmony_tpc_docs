### **getActionIndex**
>+ openharmony API: ohos.multimodalinput.event.TouchEvent.getIndex
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：直接替换

### **getActionMasked**
>+ openharmony API: ohos.multimodalinput.event.TouchEvent.getAction
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：直接替换

