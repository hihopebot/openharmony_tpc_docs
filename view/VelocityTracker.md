
### **obtain**
>+ openharmony API: ohos.agp.components.VelocityDetector.obtainInstance
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：直接替换 


### **addMovement**
>+ openharmony API: ohos.agp.components.VelocityDetector.addEvent
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
```java
VelocityDetector detector = VelocityDetector.obtainInstance();
detector.addEvent(touchEvent);
```

### **computeCurrentVelocity**
>+ openharmony API:  ohos.agp.components.VelocityDetector.calculateCurrentVelocity
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
```java
VelocityDetector detector = VelocityDetector.obtainInstance();
detector.calculateCurrentVelocity(units, maxVxVelocity, maxVyVelocity);
```
>+ 注意事项：给TouchEvent事件添加惯性（VelocityDetector）的时候必须要按如下顺序才可以正常获取到VelocityDetector的数值，否则数值会一直是0
顺序如下：
VelocityDetector detector = VelocityDetector.obtainInstance();
detector.addEvent(touchEvent);
detector.calculateCurrentVelocity(1000, 10000, 10000);
float velocity = detector.getVerticalVelocity();


### **getXVelocity**
>+ openharmony API:  ohos.agp.components.VelocityDetector.getHorizontalVelocity
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:

```java
VelocityDetector detector = VelocityDetector.obtainInstance();
// 必须先调用computeCurrentVelocity方法
detector.getHorizontalVelocity();
```


### **getYVelocity**
>+ openharmony API:  ohos.agp.components.VelocityDetector.getVerticalVelocity
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:

```java
VelocityDetector detector = VelocityDetector.obtainInstance();
// 必须先调用computeCurrentVelocity方法
detector.getVerticalVelocity();
```

### **clear**
>+ openharmony API:  ohos.agp.components.VelocityDetector.clear
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:

```java
VelocityDetector detector = VelocityDetector.obtainInstance();
detector.clear();
```