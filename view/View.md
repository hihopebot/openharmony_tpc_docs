﻿### **onMeasure**
>+ openharmony API: ohos.agp.components.Component.onEstimateSize
>+ openharmony SDK版本：2.1.0.17 以上
>+ IDE版本：2.1.0.141
>+ 实现方案：实现接口 implements Component.EstimateSizeListener，设置监听 setEstimateSizeListener(this)
 
       @Override
       public boolean onEstimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
       return true;
       }
 
>+ 补充说明：onEstimateSize在sdk 2.1.0.17以前未实现，临时方案可以通过在构造函数中调用setLayoutRefreshedListener或者addDrawTask中去获取宽高信息



### **onLayout**
>+ openharmony API: ohos.agp.components.Component.onArrange
>+ openharmony SDK版本：2.1.0.17 以上
>+ IDE版本：2.1.0.141
>+ 实现方案：实现接口 implements ComponentContainer.ArrangeListener，设置监听 setArrangeListener(this)
 
     @Override
     public boolean onArrange(int l, int t, int r, int b) {
     return true;
     }
 
>+ 补充说明：onArrange在sdk 2.1.0.17以前未实现，临时方案为，如果不涉及到View.layout()操作可以直接将逻辑放到onDraw里面，否则可以通过component.setComponentPosition()对子控件重新布局;
    
    
### **onDraw**
>+ openharmony API: ohos.agp.components.Component.addDrawTask
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：在构造函数调用addDrawTask(this)，自定义Component实现Component.DrawTask接口，重写onDraw(Component component, Canvas canvas)方法。

### **onSizeChanged**
>+ openharmony API: ohos.agp.components.Component.LayoutRefreshedListener
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：可以在构造函数或者其他地方主动调用。

### **onAttachedToWindow**
>+ openharmony API: ohos.agp.components.Component.BindStateChangedListener
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：可以在构造函数或者其他地方主动调用。

### **onDetachedFromWindow**
>+ openharmony API: ohos.agp.components.Component.BindStateChangedListener
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：可以在构造函数或者其他地方主动调用。

### **onTouchEvent**
>+ openharmony API: ohos.agp.components.Component.setTouchEventListener
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：在构造函数调用setTouchEventListener(Component.TouchEventListener listener)，自定义Component实现Component.TouchEventListener接口，重写onTouchEvent(MotionEvent event)方法。

### **onScrollChanged**

>+ openharmony API: ohos.agp.components.ComponentTreeObserver.addScrolledListener
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：在构造函数调用getComponentTreeObserver().addScrolledListener(ComponentTreeObserver.ScrollChangedListener listener)，自定义Component实现ComponentTreeObserver.ScrollChangedListener接口，onScrolled()方法。

### **invalidate**

>+ openharmony API: ohos.agp.components.Component.invalidate 
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：在onDraw()方法中调用直接invalidate()方法无效，需在asyncDispatch方法中调用，如下：getContext().getUITaskDispatcher().asyncDispatch(this::invalidate)。