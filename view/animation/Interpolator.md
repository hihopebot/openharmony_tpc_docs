﻿### **Interpolator**
>+ openharmony class: ohos.agp.animation.Animator.CurveType
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：原三方库的ValueAnimator.setInterpolator(Interpolator interpolator)用ohos.agp.animation.AnimatorValue.setCurveType(int curveType)代替，其中curveType的int值是ohos.agp.animation.Animator.CurveType类的枚举常量.