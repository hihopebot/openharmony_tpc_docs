﻿### **AnimationSet**
>+ openharmony API: ohos.agp.animation.AnimatorGroup
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用AnimatorGroup类对应

```java
原三方库:
Animation animation = new Animation();
AnimationSet.add(Animation);
view.startAnimation(AnimationSet)

openharmony :
Animation animation = new Animation(component);
AnimatorGroup animatorGroup = new AnimatorGroup();
animatorGroup.build().addAnimators(animation);
animatorProperty.start();
```


### **addAnimation**
>+ openharmony API: ohos.agp.animation.AnimatorGroup.build().addAnimators
>+ 实现方案：使用AnimatorGroup类对应

```java
Animation animation = new Animation(component);
AnimatorGroup animatorGroup = new AnimatorGroup();
animatorGroup.build().addAnimators(animation);
animatorProperty.start();
```