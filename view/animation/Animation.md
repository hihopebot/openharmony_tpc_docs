﻿### **Animation**
>+ openharmony class: ohos.agp.animation.AnimatorProperty
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
实现方案：使用AnimatorProperty类对应
原三方库:
view.startAnimation(animation)

openharmony :
AnimatorProperty animatorProperty = new AnimatorProperty(component);
animatorProperty.start();