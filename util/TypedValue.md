﻿# TypedValue
* openharmony API: ohos.agp.components.AttrHelper
* openharmony SDK版本：2.0.1.93及以上
* IDE版本：2.0.12.131
* 实现方案：
## fp转px
```java
    public static float fpToPixels(Context context, float value) {
        return  AttrHelper.fp2px(value,context);
    }
 ```
##  vp转px
```java
  public static float vpToPixels(Context context, float value) {
        return  AttrHelper.vp2px(value,context);
    }
 ```