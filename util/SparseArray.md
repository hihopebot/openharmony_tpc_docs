### **put**
>+ openharmony API: ohos.utils.PlainArray.put
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
PlainArray<String> plainArray = new PlainArray<>();
plainArray.put(1, "A");
```

### **append**
>+ openharmony API: ohos.utils.PlainArray.append
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
PlainArray<String> plainArray = new PlainArray<>();
plainArray.append(2,"B");
```

### **remove**
>+ openharmony API: ohos.utils.PlainArray.remove
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
PlainArray<String> plainArray = new PlainArray<>();
plainArray.remove(1);
```

### **removeAt**
>+ openharmony API: ohos.utils.PlainArray.removeAt
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
PlainArray<String> plainArray = new PlainArray<>();
plainArray.removeAt(0);
```

### **contains**
>+ openharmony API: ohos.utils.PlainArray.contains
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
PlainArray<String> plainArray = new PlainArray<>();
plainArray.contains(1);
```

### **clear**
>+ openharmony API: ohos.utils.PlainArray.clear
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
PlainArray<String> plainArray = new PlainArray<>();
plainArray.clear();
```
