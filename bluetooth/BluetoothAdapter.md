### **disable**
>+  openharmony API:ohos.bluetooth.BluetoothHost.disableBt
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：BluetoothHost.disableBt()

### **enable**
>+  openharmony API:ohos.bluetooth.BluetoothHost.enableBt
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：BluetoothHost.enableBt()

>+  补充说明：调用enableBt()前需要判断蓝牙状态，直接调用hui会弹出蓝牙开关的对话框
    if (bluetoothHost.getBtState()== BluetoothHost.STATE_OFF){
        //此API会弹出蓝牙开关的对话框,需要配置"ohos.permission.DISCOVER_BLUETOOTH"权限
        bluetoothHost.enableBt();
    }

### **cancelDiscovery**
>+  openharmony API:ohos.bluetooth.BluetoothHost.cancelBtDiscovery
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：bluetoothHost.cancelBtDiscovery()

### **checkBluetoothAddress**
>+  openharmony API:ohos.bluetooth.BluetoothHost.isValidBluetoothAddr
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：bluetoothHost.isValidBluetoothAddr(String)

### **isEnabled**
>+  openharmony API:ohos.bluetooth.BluetoothHost.getBtState
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：使用bluetoothHost.getBtState()获取蓝牙状态，再调用enableBt()方法

### **getName**
>+  openharmony API:ohos.bluetooth.BluetoothHost.getLocalName
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：bluetoothHost.getLocalName()




