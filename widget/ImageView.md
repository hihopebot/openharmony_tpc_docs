### **setColorFilter**
>+  openharmony API:暂无
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：自行实现,使用方法如下:
```java
public static void setImageTint(Element element, int intColor, BlendMode mode, Image image) {
        int[] colors = new int[]{intColor, intColor};
        int[][] states = new int[2][];
        states[0] = new int[]{0};
        states[1] = new int[]{0};
        element.setStateColorList(states, colors);
        element.setStateColorMode(mode);
        image.setImageElement(element);
    }

```