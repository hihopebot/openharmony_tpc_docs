﻿+ openharmony API: 无
+ openharmony SDK版本：2.1.0.17
+ IDE版本：2.1.0.141
+ 实现方案：
  + 1.先复制下面这个自定义Provider到你的目录下:
``` java
public abstract class EasyGridProvider<T> extends RecycleItemProvider {

    private Context context;
    private List<T> data;
    private int mLayoutId;
    private int numColumns = 1;
    private OnItemClickListener onItemClickListener;

    /**
     * @param context   上下文
     * @param data      数据源
     * @param mLayoutId 条目的资源文件id
     * @return
     */
    public EasyGridProvider(Context context, int mLayoutId, List<T> data) {
        this.context = context;
        this.data = data;
        this.mLayoutId = mLayoutId;
    }

    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
    }

    public void setData(List<T> data) {
        this.data = data;
        notifyDataChanged();
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public int getCount() {
        if (data != null) {
            return data.size() % numColumns == 0 ? data.size() / numColumns : data.size() / numColumns + 1;
        } else {
            return 0;
        }
    }

    @Override
    public T getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        ViewHolder viewHolder;
        convertComponent = new DirectionalLayout(context);
        ((DirectionalLayout) convertComponent).setOrientation(Component.HORIZONTAL);
        ComponentContainer.LayoutConfig layoutConfig = convertComponent.getLayoutConfig();
        layoutConfig.width = DependentLayout.LayoutConfig.MATCH_PARENT;
        layoutConfig.height = DependentLayout.LayoutConfig.MATCH_CONTENT;
        convertComponent.setLayoutConfig(layoutConfig);
        for (int i = 0; i < numColumns; i++) {
            if (position * numColumns + i < data.size()) {
                DirectionalLayout dlItemParent = new DirectionalLayout(context);
                dlItemParent.setLayoutConfig(new DirectionalLayout.LayoutConfig(0, DirectionalLayout.LayoutConfig.MATCH_CONTENT, LayoutAlignment.TOP, 1));
                Component childConvertComponent = LayoutScatter.getInstance(context).parse(mLayoutId, null, false);
                int finalI = i;
                childConvertComponent.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(component, position * numColumns + finalI);
                        }
                    }
                });
                dlItemParent.addComponent(childConvertComponent);
                ((ComponentContainer) convertComponent).addComponent(dlItemParent);
                viewHolder = new ViewHolder(childConvertComponent);
                bind(viewHolder, getItem(position * numColumns + i), position * numColumns + i);
            } else {
                //用Component占位会导致高度为MATCH_PARENT,所以此处用DirectionalLayout占位
                DirectionalLayout childConvertComponent = new DirectionalLayout(context);
                childConvertComponent.setLayoutConfig(new DirectionalLayout.LayoutConfig(0, DirectionalLayout.LayoutConfig.MATCH_CONTENT, LayoutAlignment.TOP, 1));
                ((ComponentContainer) convertComponent).addComponent(childConvertComponent);
            }
        }
        return convertComponent;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    protected abstract void bind(ViewHolder holder, T s, int position);

    protected static class ViewHolder {
        HashMap<Integer, Component> mViews = new HashMap<>();
        public Component itemView;

        ViewHolder(Component component) {
            this.itemView = component;
        }

        public ViewHolder setText(int viewId, String text) {
            ((Text) getView(viewId)).setText(text);
            return this;
        }

        public <E extends Component> E getView(int viewId) {
            E view = (E) mViews.get(viewId);
            if (view == null) {
                view = (E) itemView.findComponentById(viewId);
                mViews.put(viewId, view);
            }
            return view;
        }

    }

    public static abstract class OnItemClickListener {
        public abstract void onItemClick(Component component, int position);
    }
}
```
  + 2.用法如下所示：
``` java
//找到ListContainer控件
ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
//创建适配器
EasyGridProvider easyGridProvider = new EasyGridProvider<String>(getContext(), ResourceTable.Layout_item_list, data1) {
    @Override
    protected void bind(ViewHolder holder, String s, int position) {
        //在这里做你想给每一个条目做的逻辑
        //设置文本的两种方式，目前只有Text支持第一种方式
        holder.setText(ResourceTable.Id_text, s);
        holder.<Text>getView(ResourceTable.Id_text).setText(s);
    }
};
//设置列数
easyGridProvider.setNumColumns(3);
//设置条目点击事件
easyGridProvider.setOnItemClickListener(new EasyGridProvider.OnItemClickListener() {
		@Override
		public void onItemClick(Component component, int position) {

		}
});
//把适配器设置给ListContainer控件
listContainer.setItemProvider(easyGridProvider);
//变更数据
easyGridProvider.setData(data2);
```
+ **注意：使用此方案后，ListContainer的条目点击事件将失效，替换成EasyGridProvider的条目点击事件即可**