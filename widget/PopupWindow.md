
# PopupDialog
* openharmony API: ohos.agp.window.dialog.PopupDialog;
* openharmony SDK版本：2.1.1.21
* IDE版本：2.1.0.501
* 实现方案:

openharmony：

``` java
  PopupDialog popupDialog = new PopupDialog(context,null);
       popupDialog.setCustomComponent(listView);
       popupDialog.setDialogListener(new BaseDialog.DialogListener() {
         @Override
         public boolean isTouchOutside() {
              return true;
         }
  });
```