### **LayoutParams**
>+  openharmony API:ohos.agp.components.StackLayout.LayoutConfig
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：替换使用,使用方法如下:
```java
    StackLayout.LayoutConfig layoutParams = new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
    layoutParams.setMarginLeft(borderMargin);
    layoutParams.setMarginTop(topMargin);
    layoutParams.setMarginRight(borderMargin);
    layoutParams.setMarginBottom(bottomMargin);
    view.setLayoutConfig(layoutParams);
```