﻿+ openharmony API: 无
+ openharmony SDK版本：2.1.0.17
+ IDE版本：2.1.0.141
+ 实现方案：
  + 1.自己编码实现类似AdapterView，这里只给出实现AdapterView最基本的功能
``` java
public abstract class ProviderComponent extends ComponentContainer
        implements Component.LayoutRefreshedListener, Component.BindStateChangedListener, Component.DrawTask {
    /**
     * item最大显示数
     */
    public static final int DEFAULT_MAX_VISIBLE = 4;

    private static final String MAX_VISIBLE_NUM = "maxVisibleNum";

    private int mMaxVisible;

    private final List<Component> mChildComponents = new ArrayList<>();

    private BaseItemProvider mProvider;

    private boolean mIsRegistered;

    /**
     * 构造方法
     *
     * @param context 上下文
     */
    public ProviderComponent(Context context) {
        this(context, null);
    }

    /**
     * 构造方法
     *
     * @param context 上下文
     * @param attrSet 自定义属性
     */
    public ProviderComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造方法
     *
     * @param context 上下文
     * @param attrSet 自定义属性
     * @param styleName 风格样式
     */
    public ProviderComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mMaxVisible = AttrUtils.getIntFromAttr(attrSet, MAX_VISIBLE_NUM, DEFAULT_MAX_VISIBLE);
        setBindStateChangedListener(this);
        setLayoutRefreshedListener(this);
        addDrawTask(this);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        if (mProvider != null && !mIsRegistered) {
            mIsRegistered = true;
            mProvider.addDataSubscriber(subscriber);
        }
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        if (mProvider != null && mIsRegistered) {
            mIsRegistered = false;
            mProvider.removeDataSubscriber(subscriber);
        }
    }

    @Override
    public Component getComponentAt(int index) {
        if (index < 0 || index >= mChildComponents.size()) {
            return null;
        }
        return mChildComponents.get(index);
    }

    @Override
    public void onRefreshed(Component component) {
        for (int i = 0; i < getChildCount(); i++) {
            Component childComponent = getComponentAt(i);
            if (childComponent != null) {
                childComponent.setContentPositionX((getWidth() - childComponent.getWidth()) / 2f);
                childComponent.setContentPositionY((getHeight() - childComponent.getHeight()) / 2f);
            }
        }
    }

    /**
     * 获取当前被选中的item组件
     *
     * @return 被选中的组件
     */
    public abstract Component getSelectedComponent();

    /**
     * 获取适配器
     *
     * @return 适配器
     */
    public BaseItemProvider getItemProvider() {
        return mProvider;
    }

    /**
     * 获取最大显示child component数
     *
     * @return child component
     */
    public int getMaxVisible() {
        return mMaxVisible;
    }

    /**
     * 设置最大显示child component数
     *
     * @param maxVisible 最大显示child component数
     */
    public void setMaxVisible(int maxVisible) {
        this.mMaxVisible = maxVisible;
    }

    /**
     * 设置适配器
     *
     * @param itemProvider 适配器
     */
    public void setItemProvider(BaseItemProvider itemProvider) {
        mProvider = itemProvider;
        if (mProvider != null && !mIsRegistered) {
            mIsRegistered = true;
            mProvider.addDataSubscriber(subscriber);
        }
        mChildComponents.clear();
        if (getChildCount() > 0) {
            removeAllComponents();
        }
        addAllItem();
    }

    private void addAllItem() {
        if (mProvider == null) {
            return;
        }
        for (int i = 0; i < mProvider.getCount(); i++) {
            boolean isAddToParent = false;
            Component childComponent = null;
            if (i < getChildCount()) {
                childComponent = getComponentAt(i);
            }
            if (childComponent == null) {
                isAddToParent = true;
            }
            childComponent = mProvider.getComponent(i, childComponent, this);
            if (isAddToParent) {
                mChildComponents.add(childComponent);
                if (getChildCount() < mMaxVisible) {
                    addComponent(childComponent, 0);
                }
            }
        }
    }

    private void insertItem(int position) {
        Component addComponent = mProvider.getComponent(position, null, this);
        mChildComponents.add(addComponent);
        for (int i = 0; i < mChildComponents.size(); i++) {
            mProvider.getComponent(i, mChildComponents.get(i), this);
        }
    }

    private void removeItem(int position) {
        if (position >= 0 && position < mChildComponents.size()) {
            Component removeComponent = mChildComponents.get(position);
            mChildComponents.remove(removeComponent);
            int childIndex = getChildIndex(removeComponent);
            if (childIndex >= 0) {
                removeComponentAt(childIndex);
            }
            int childCount = getChildCount();
            if (childCount < mMaxVisible && childCount < mChildComponents.size()) {
                addComponent(mChildComponents.get(getChildCount()), 0);
            }
        }
    }

    private DataSetSubscriber subscriber =
            new DataSetSubscriber() {
                @Override
                public void onChanged() {}

                @Override
                public void onItemInserted(int position) {
                    insertItem(position);
                }

                @Override
                public void onItemRemoved(int position) {
                    removeItem(position);
                }

                @Override
                public void onItemRangeChanged(int positionStart, int itemCount) {}

                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {}

                @Override
                public void onItemRangeRemoved(int positionStart, int itemCount) {}
            };
}
```

  + 2.使用:

    1、xml布局加入：
``` java
    <com.lorentzos.flingswipe.ProviderComponent
        ohos:id="$+id:frame"
        ohos:width="match_parent"
        ohos:height="match_parent"/>
```

    2、写ProviderComponent的适配器：
``` java
public class BaseAdapter extends BaseItemProvider {
    private Context mContext;

    private List<String> mDataList;

    /**
     * constructor
     *
     * @param context context
     */
    public BaseAdapter(Context context) {
        mContext = context;
    }

    /**
     * set data in the list
     *
     * @param dataList data source
     */
    public void setData(List<String> dataList) {
        this.mDataList = dataList;
        super.notifyDataChanged();
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public String getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer container) {
        MyComponentHolder holder;
        if (convertView == null) {
            convertView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item, container, false);
            holder = new MyComponentHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (MyComponentHolder) convertView.getTag();
        }
        Text text = (Text) holder.getItemView().findComponentById(ResourceTable.Id_helloText);
        text.setText(mDataList.get(position));
        return convertView;
    }

    /**
     * item component holder
     */
    public static class MyComponentHolder {
        private final Component itemView;

        public MyComponentHolder(Component itemView) {
            if (itemView == null) {
                throw new IllegalArgumentException("itemView may not be null");
            } else {
                this.itemView = itemView;
            }
        }

        public Component getItemView() {
            return itemView;
        }
    }
}
```

    3、与适配器绑定：
``` java
        BaseAdapter provider = new BaseAdapter(this);
        provider.setData(al);
        ProviderComponent container = (ProviderComponent) findComponentById(ResourceTable.Id_frame);
        container.setItemProvider(provider);
```

    4、刷新数据接口
``` java
    插入数据：
        al.add("XML ".concat(String.valueOf(mCount)));
        provider.notifyDataSetItemInserted(al.size() - 1);
    
    删除数据：
        al.remove(0);
        provider.notifyDataSetItemRemoved(0);
```