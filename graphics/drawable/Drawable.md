﻿### **draw(Canvas canvas)**
>+ openharmony API: ohos.agp.components.element.Element.drawToCanvas
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：

```java 
   1：继承ShapeElemen，并重写drawToCanvas
   2：外部通过addDrawTask来手动触发drawToCanvas的回调。
      Component.addDrawTask(new Component.DrawTask() {
          @Override
          public void onDraw(Component component, Canvas canvas) {
              customDrawable = new CustomDrawable();
              customDrawable.drawToCanvas(canvas);
                  }
           });
```
>+ 补充说明：目前直接继承Element会报错，请继承其子类。
原三方库中构造自定义Drawable后会自动调用draw（）方法，但是drawToCanvas无法自动调用，
需要在外部主动调用才执行，目前验证可行的方案是在自定义Component的addDrawTask中手动调用，在其他的非自定义控件场景中暂时未找到触发回调的办法，自定义element后续会支持并完善。