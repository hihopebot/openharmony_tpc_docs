﻿### vector.xml转换VectorElement
+ openharmony API: `ohos.agp.components.element.VectorElement`
+ openharmony SDK版本：2.0.1.93 以上
+ IDE版本：2.0.12.141
+ 实现方案：
	+ 1.定义一个xml文件: `vector_test.xml`
	```java
	<vector xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:width="24vp"
        ohos:height="24vp"
        ohos:tint="$color:appintro_icon_tint"
        ohos:viewportWidth="24.0"
        ohos:viewportHeight="24.0">
    <path
        ohos:fillColor="#FF00FF"
        ohos:pathData="M12,4l-1.41,1.41L16.17,11H4v2h12.17l-5.58,5.59L12,20l8,-8z" />
	</vector>
	```
	+ 2.在java代码中使用已经定义好的xml文件：
	```java
	Image image ;
	VectorElement vectorElement = new VectorElement(this, Graphic_vector_test);
	image.setImageElement(vectorElement);
	or
	image.setBackground(vectorElement);
	```