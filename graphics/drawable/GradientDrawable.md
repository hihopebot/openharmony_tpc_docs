﻿### **GradientDrawable()**
>+ openharmony API: `ohos.agp.components.element.ShapeElement`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
	```java
	ShapeElement shapeElement = new ShapeElement();
	```
### **setCornerRadius(float radius)**
>+ openharmony API: `ohos.agp.components.element.ShapeElement`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
	```java
	shapeElement.setShape(ShapeElement.RECTANGLE);//根据不同的形状传不同的参数
	shapeElement.setCornerRadius(raduis);
	```
### **setStroke(int width, int color)**
>+ openharmony API: `ohos.agp.components.element.ShapeElement`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
	```java
	shapeElement.setStroke(strokeWidth, new RgbColor(strokeColor));
	```
### **setColor(int argb)**
>+ openharmony API: `ohos.agp.components.element.ShapeElement`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
	```java
	shapeElement.setRgbColor(new RgbColor(color));
	shapeElement.setAlpha(alpha);
	```

### **setCornerRadii**
>+ openharmony API: `ohos.agp.components.element.ShapeElement`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
	```java
	ShapeElement leftItemPressedDrawable = new ShapeElement();
	leftItemPressedDrawable.setCornerRadiiArray(new float[]{});
	```