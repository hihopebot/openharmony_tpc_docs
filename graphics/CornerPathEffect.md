### **CornerPathEffect(float)**
>+ openharmony API: `ohos.agp.render.PathEffect`
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
paint.setPathEffect(new PathEffect(10f));
```