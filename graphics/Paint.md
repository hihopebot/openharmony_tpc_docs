﻿### **setShadowLayer**
>+ openharmony class: ohos.agp.render.Paint
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替代对比如下

原三方库：
```java
paint.setShadowLayer(shadowRadius, shadowDx, shadowDy, shadowColor)
```
openharmony :
```java
BlurDrawLooper textBlurDrawLooper = new BlurDrawLooper(shadowRadius, shadowDx, shadowDy, new Color(shadowColor));
            paint.setBlurDrawLooper(textBlurDrawLooper);
```