﻿### **decodeRegion**
>+ openharmony API: `ohos.media.image.PixelMap`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案和步骤：使用`PixelMap.create()`方法
``` java
public static PixelMap decodeRegion(PixelMap source,Rect srcRegion,PixelMap.InitializationOptions opts){
   return PixelMap.create(source,srcRegion,opts);
}
```