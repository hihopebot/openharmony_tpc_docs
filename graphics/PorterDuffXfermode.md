### **PorterDuffXfermode(PorterDuff.Mode)**
>+ openharmony API: ohos.agp.render.Canvas.PorterDuffMode
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Canvas canvas = new Canvas();
// 根据指定的混合模式画指定颜色
canvas.drawColor(Color.BLUE.getValue(), Canvas.PorterDuffMode.SRC_OVER);
```