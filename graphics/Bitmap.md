﻿openharmony 替换类`ohos.media.image.PixelMap`

根据来源创建`PixelMap`
```java
ImageSource.SourceOptions options = new ImageSource.SourceOptions();
ImageSource source = ImageSource.create(data, options);
ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
PixelMap pixelMap = source.createPixelmap(decodingOptions);
```
直接创建初始选项的图片
```java
PixelMap.InitializationOptions opts = new PixelMap.InitializationOptions();
opts.size = new Size(100,100);
opts.pixelFormat = PixelFormat.ARGB_8888;
PixelMap pixelMap = PixelMap.create(opts);
```

### **createBitmap(source,x, y, width, height,matrix,filter)**
>+ openharmony API: ohos.media.image.PixelMap
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：暂无直接替换，需要自己去写，使用方法如下:
```java
public static PixelMap createPixelMap(PixelMap source, int x, int y, int width, int height,
            Matrix matrix, boolean filter){
PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();

RectFloat srcR = new RectFloat(x, y, x + width,y + height);
RectFloat dstR = new RectFloat(0, 0, width, height);
RectFloat deviceR = new RectFloat();
Paint paint = new Paint();
paint.setFilterBitmap(filter);
final boolean transformed = !matrix.rectStaysRect();
if (transformed) {
  paint.setAntiAlias(true);
}
matrix.mapRect(deviceR, dstR);

int neww = Math.round(deviceR.getWidth());
int newh = Math.round(deviceR.getHeight());
initializationOptions.size = new Size(neww,newh);
initializationOptions.scaleMode = ScaleMode.CENTER_CROP;
initializationOptions.alphaType = AlphaType.OPAQUE;
PixelMap srcPixelMap = PixelMap.create(initializationOptions);
Texture texture = new Texture(srcPixelMap);
Canvas canvas = new Canvas(texture);

canvas.translate(-deviceR.left, -deviceR.top);
canvas.concat(matrix);
canvas.drawPixelMapHolderRect(new PixelMapHolder(source), srcR, dstR, paint);
return srcPixelMap/texture.getPixelMap();
}

```


### **getWidth()**
>+ openharmony API: ohos.media.image.common.Size
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
```java
int width = pixelMap.getImageInfo().size.width;
```

### **getHeight()**
>+ openharmony API: ohos.media.image.common.Size
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用,使用方法如下:
```java
int height = pixelMap.getImageInfo().size.height;
```
### **getByteCount()**
>+ openharmony API: ohos.media.image.PixelMap.getPixelBytesCapacity();
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用

### **getDensity()**
>+ openharmony API: ohos.media.image.PixelMap.getBaseDensity();
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用

### **recycle()**
>+ openharmony API: ohos.media.image.PixelMap.release()
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替换使用