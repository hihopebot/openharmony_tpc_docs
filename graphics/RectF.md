### **RectF**
>+ openharmony API: ohos.agp.utils.RectFloat
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
// 通过坐标点直接构建
RectFloat rectFloat = new RectFloat(0f, 0f, 100f, 100f);

// 通过传入一个RectFloat进行构建
RectFloat rectFloat2 = new RectFloat(new RectFloat(0f, 0f, 100f, 100f));
```

### **width**
>+ openharmony API: ohos.agp.utils.RectFloat.getWidth
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
RectFloat rectFloat = new RectFloat(0f, 0f, 100f, 100f);
rectFloat.getWidth();
```

### **height**
>+ openharmony API: ohos.agp.utils.RectFloat.getHeight
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
RectFloat rectFloat = new RectFloat(0f, 0f, 100f, 100f);
rectFloat.getHeight();
```

### **centerX**
>+ openharmony API: ohos.agp.utils.RectFloat.getHorizontalCenter
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
RectFloat rectFloat = new RectFloat(0f, 0f, 100f, 100f);
rectFloat.getHorizontalCenter();
```

### **centerY**
>+ openharmony API: ohos.agp.utils.RectFloat.getVerticalCenter
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
RectFloat rectFloat = new RectFloat(0f, 0f, 100f, 100f);
rectFloat.getVerticalCenter();
```

### **contains**
>+ openharmony API: ohos.agp.utils.RectFloat.isInclude
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
RectFloat rectFloat = new RectFloat(0f, 0f, 100f, 100f);
// 方式一
rectFloat.isInclude(50f, 50f);
// 方式二
rectFloat.isInclude(new Point(50f, 50f));
// 方式三
rectFloat.isInclude(new RectFloat(0f, 0f, 50f, 50f));
// 方式四
rectFloat.isInclude(0f, 0f, 50f, 50f);
```

