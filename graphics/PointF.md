### **PointF(float, float)**
>+ openharmony API: ohos.agp.utils.Point(float, float)
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
// 直接构建
Point point = new Point(100f, 100f);
```

### **set**
>+ openharmony API: ohos.agp.utils.Point.modify
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Point point = new Point();
// 重新修改坐标点
point.modify(100f, 100f);
```
