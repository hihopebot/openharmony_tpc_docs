### **moveTo**
>+ openharmony API: ohos.agp.render.Path.moveTo
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Path path = new Path();
path.moveTo(100f, 100f);

```

### **lineTo**
>+ openharmony API: ohos.agp.render.Path.lineTo
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Path path = new Path();
path.lineTo(200f, 200f);

```

### **setLastPoint**
>+ openharmony API: ohos.agp.render.Path.setLastPoint
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Path path = new Path();
path.setLastPoint(100f, 100f);
// 或者
path.setLastPoint(new Point(100f, 100f));

```

### **addRect**
>+ openharmony API: ohos.agp.render.Path.addRect
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Path path = new Path();
path.addRect(new RectFloat(0f, 0f, 100f, 100f), Path.Direction.CLOCK_WISE);

```
