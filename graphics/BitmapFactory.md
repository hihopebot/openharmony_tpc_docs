﻿### **decodeResource**
>+ openharmony API: 无
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：
``` java
public static PixelMap decodeResource(Context context, int id) {
        try {
            String path = context.getResourceManager().getMediaPath(id);
            if (path.isEmpty()) {
                return null;
            }
            RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            options.formatHint = "image/png";
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions)).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
```

### **decodeStream**
>+ openharmony API: `ohos.media.image.ImageSource`>
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：自定义
``` java
public static PixelMap decodeStream(InputStream is) {
    ImageSource.SourceOptions options = new ImageSource.SourceOptions();
    ImageSource source = ImageSource.create(is,options);//options可为null
    ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
    return source.createPixelmap(decodingOptions);//decodingOptions可为null
}
```


### **decodeFileDescriptor**
>+ openharmony API: `ohos.media.image.ImageSource`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：自定义
``` java
public static PixelMap decodeStream(FileDescriptor fd) {
    ImageSource.SourceOptions options = new ImageSource.SourceOptions();
    ImageSource source = ImageSource.create(fd,options);//options可为null
    ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
    return source.createPixelmap(decodingOptions);//decodingOptions可为null
}
```