### **setScale**
>+ openharmony API: ohos.agp.utils.Matrix.setScale
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Matrix matrix = new Matrix();
// 默认以左上角的点进行缩放矩阵
matrix.setScale(0.5f, 0.5f);
// 以坐标点（100,100）为枢轴点进行缩放矩阵
matrix.setScale(0.5f, 0.5f, 100f, 100f);

```

### **setTranslate**
>+ openharmony API: ohos.agp.utils.Matrix.setTranslate
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Matrix matrix = new Matrix();
// 平移指定的距离
matrix.setTranslate(100f, 100f);

```

### **setSkew**
>+ openharmony API: ohos.agp.utils.Matrix.setSkew
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Matrix matrix = new Matrix();
// 设置水平错切和垂直错切的值为 0.3
matrix.setSkew(0.3f, 0.3f);

```

### **setRotate**
>+ openharmony API: ohos.agp.utils.Matrix.setRotate
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
Matrix matrix = new Matrix();
// 默认以左上角为锚点进行旋转90度
matrix.setRotate(90);
// 以指定坐标点进行旋转90度
matrix.setRotate(90, 100f, 100f);

```

