﻿
### **Canvas(Bitmap)**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`Canvas(Texture)`方法
``` java
Canvas canvas = new Canvas(new Texture(pixelMap));
```

### **drawBitmap(Bitmap,Rect, RectF, Paint)**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`drawPixelMapHolderRect(PixelMapHolder, RectFloat, RectFloat , Paint )`方法

### **quickReject()**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`quickRejectRect(RectFloat)` , `quickRejectPath(Path)`方法

### **clipPath()**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`clipPath(Path, Canvas.ClipOp)`方法

### **clipRect()**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`clipRect(RectFloat, Canvas.ClipOp)` , `clipRect(float, float, float, float)` , `clipRect(RectFloat, Canvas.ClipOp)` , `clipRect(int, int, int, float)`方法

### **concat()**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`concat(Matrix)`方法

### **drawArc()**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`drawArc(RectFloat, Arc, Paint)`方法

### **drawARGB()**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案和步骤：使用`drawColor(int, Canvas.PorterDuffMode)`方法

### **getDensity()**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getDeviceDensity(Context)`方法

### **getSaveCount()**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getSaveCount()`方法

### **drawLine(float startX, float startY, float stopX, float stopY,Paint paint)**
>+ openharmony API: `ohos.agp.render.Canvas`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`drawLine(Point point1 ,Point point2,Paint)`方法