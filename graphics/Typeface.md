﻿### **createFromAsset**
>+ openharmony class: ohos.agp.text.Font
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替代对比如下

原三方库:
```java
mTypeface = Typeface.createFromAsset(context.getResources().getAssets(), typeface);
```
openharmony :
```java
Font createFontBuild(Context context, String name) {
        ResourceManager resManager = context.getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/" + name);
        Resource resource = null;
        try {
            resource = rawFileEntry.openRawFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuffer fileName = new StringBuffer(name);
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName.toString());
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            int index;
            byte[] bytes = new byte[1024];
            while ((index = resource.read(bytes)) != -1) {
                outputStream.write(bytes, 0, index);
                outputStream.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                resource.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        Font.Builder builder = new Font.Builder(file);
        return builder.build();
    }
```  

### **create**
>+ openharmony class: ohos.agp.text.Font.Build
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：替代对比如下

原三方库:
```java
Typeface.create(mTypeface, mTextStyle)
```
openharmony :
```java
switch (mTextStyle) {
    case NORMAL:
        mTypeface.setWeight(Font.REGULAR);
        mTypeface.makeItalic(false);
        break;
    case BOLD:
        mTypeface.setWeight(Font.BOLD);
        mTypeface.makeItalic(false);
        break;
    case ITALIC:
        mTypeface.setWeight(Font.REGULAR);
        mTypeface.makeItalic(true);
        break;
    case BOLD_ITALIC:
        mTypeface.setWeight(Font.BOLD);
        mTypeface.makeItalic(true);
        break;
}
```  