﻿### **setXfermode**
>+  openharmony API:ohos.agp.render.BlendMode
>+  openharmony SDK版本：2.1.0.17
>+  IDE版本：2.1.0.141
>+  实现方案，替代对比如下

原三方库 :
```java
paint.setXfermode(xfermode);
```
openharmony :
```java
paint.setBlendMode(BlendMode.SRC_OUT);
```

具体枚举解释查看API文档