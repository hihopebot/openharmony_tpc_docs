
# PixelMapShader
* openharmony API: ohos.agp.render.PixelMapShader
* openharmony SDK版本：2.1.1.21
* IDE版本：2.1.0.501
* 实现方案:


原三方库：

BitmapShader通过设置给mPaint，然后用这个mPaint绘图时，就会根据你设置的TileMode，对绘制区域进行着色
``` java
   // 渲染图像，使用图像为绘制图形着色
   private BitmapShader mBitmapShader;  
   // 将bmp作为着色器，就是在指定区域内绘制bmp  
   mBitmapShader = new BitmapShader(bmp, TileMode.CLAMP, TileMode.CLAMP); 
   // 设置变换矩阵  
   mBitmapShader.setLocalMatrix(mMatrix);  
   // 设置shader  
   mBitmapPaint.setShader(mBitmapShader);  
```

openharmony：

当您需要绘制的对象大于像素图着色器定义的着色区域时，您可以为像素图指定平铺模式以绘制重复或镜像的像素图。
``` java
   PixelMapShader mShadel;
   mShadel = new PixelMapShader(new PixelMapHolder(pixelMap), Shader.TileMode.CLAMP_TILEMODE,Shader.TileMode.CLAMP_TILEMODE);
```