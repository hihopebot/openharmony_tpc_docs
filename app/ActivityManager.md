﻿
### **MemoryInfo**
>+ openharmony class：`ohos.aafwk.ability.SystemMemoryInfo`
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：

* 原库实现
```java
MemoryInfo memoryInfo = new MemoryInfo() ;  
mActivityManager.getMemoryInfo(memoryInfo) ;  
```
* 替换实现
```java
SystemMemoryInfo systemMemoryInfo = new SystemMemoryInfo();
context.getAbilityManager().getSystemMemoryInfo(systemMemoryInfo);    
```

