﻿### **onConfigurationChanged 监听横竖屏切换**
>+ openharmony class：`ohos.aafwk.ability.Ability`
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：

步骤一：在 `config.json` 中对 ability 增加属性配置
```json
"abilities": [
    {        
      "configChanges": [
        "orientation"
      ],        
    }
```

步骤二： 监听方向切换
```java
 @Override
protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        if (displayOrientation.name().equals("PORTRAIT")) {
            //
        } else if (displayOrientation.name().equals("LANDSCAPE")) {
            //
        }
    }

```
