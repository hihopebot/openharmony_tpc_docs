﻿### **getCount()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getRowCount()`方法

### **getPosition()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getRowIndex()`方法

### **move(int)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`goTo(int)`方法

### **moveToPosition(int)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`goToRow(int)`方法

### **moveToFirst()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`goToFirstRow()`方法


### **moveToLast()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`goToLastRow()`方法

### **moveToNext()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`goToNextRow()`方法

### **moveToPrevious()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`goToPreviousRow()`方法

### **getColumnIndex(String)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getColumnIndexForName(String)`方法

### **getColumnIndexOrThrow(String)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getColumnIndexForName(String)`方法

### **getColumnName(int)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getColumnNameForIndex(int)`方法

### **getColumnCount()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getColumnCount()`方法

### **getColumnNames()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getAllColumnNames()`方法

### **registerDataSetObserver(DataSetObserver)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`registerObserver(DataObserver)`方法

### **unregisterDataSetObserver(DataSetObserver)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`unregisterObserver(DataObserver)`方法

### **setNotificationUris(ContentResolver,List<Uri>)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`setAffectedByUris(Object,List<Uri>)`方法

### **setExtras(Bundle)**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`setExtensions(PacMap)`方法

### **getExtras()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getExtensions()`方法

### **close()**
>+ openharmony API: `ohos.data.resultset.ResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`close()`方法