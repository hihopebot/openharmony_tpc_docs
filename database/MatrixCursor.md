﻿
### **getCount()**
>+ openharmony API: `ohos.data.resultset.TableResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getRowCount()`方法


### **getInt()**
>+ openharmony API: `ohos.data.resultset.TableResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getInt(int)`方法

### **getLong()**
>+ openharmony API: `ohos.data.resultset.TableResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getLong(int)`方法

### **getString()**
>+ openharmony API: `ohos.data.resultset.TableResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getString(int)`方法

### **getColumnNames()**
>+ openharmony API: `ohos.data.resultset.TableResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`getAllColumnNames()`方法

### **isNull()**
>+ openharmony API: `ohos.data.resultset.TableResultSet`
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：使用`isColumnNull(int)`方法