﻿#### **query()**
>+ openharmony API: `ohos.data.rdb.RdbStore.query()`
>+ openharmony SDK版本：2.1.0.17 以上
>+ IDE版本：2.1.0.141
>+ 实现方案和步骤：使用`query(AbsRdbPredicates var1, String[] var2)`方法

``` java
RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(getTableName());
            rawRdbPredicates.setWhereClause(selection);
            if (selectionArgs == null || selectionArgs.length == 0) {
                rawRdbPredicates.setWhereArgs(null);
            } else {
                rawRdbPredicates.setWhereArgs(Arrays.asList(selectionArgs));
            }
            cursor = database.query(rawRdbPredicates, columns);
            while (!cursor.isClosed() && cursor.goToNextRow()) {
                list.add(parseCursorToBean(cursor));
            }
            database.markAsCommit();
```
            
            
            
            
            
#### **update()**
>+ openharmony API: `ohos.data.rdb.RdbStore.update()`
>+ openharmony SDK版本：2.1.0.17 以上
>+ IDE版本：2.1.0.141
>+ 实现方案和步骤：使用`update(ValuesBucket var1, AbsRdbPredicates var2)`方法
``` java
			database.beginTransaction();
            RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(getTableName());
            rawRdbPredicates.setWhereClause(whereClause);
            if (whereArgs == null || whereArgs.length == 0) {
                rawRdbPredicates.setWhereArgs(null);
            } else {
                rawRdbPredicates.setWhereArgs(Arrays.asList(whereArgs));
            }
            database.update(contentValues, rawRdbPredicates);
            database.markAsCommit();
```

#### **delete()**
>+ openharmony API: `ohos.data.rdb.RdbStore.delete()`
>+ openharmony SDK版本：2.1.0.17 以上
>+ IDE版本：2.1.0.141
>+ 实现方案和步骤：使用` delete(String whereClause, String[] whereArgs) `方法
``` java
			database.beginTransaction();
            RawRdbPredicates rdbPredicates = new RawRdbPredicates(getTableName());
            rdbPredicates.setWhereClause(whereClause);
            if (whereArgs == null || whereArgs.length == 0) {
                rdbPredicates.setWhereArgs(null);
            } else {
                rdbPredicates.setWhereArgs(Arrays.asList(whereArgs));
            }
            database.delete(rdbPredicates);
            database.markAsCommit();
```
