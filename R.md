﻿### **R.id.content**
>+ openharmony API: 无
>+ openharmony SDK版本：2.1.0.17 
>+ IDE版本：2.1.0.141
>+ 实现方案：R.id.content一般用于获取根布局，暂无对应的获取根布局的接口，可以通过暴露接口或增加构造参数让开发者传入布局作为根布局，然后调用根布局对象的getComponentParent()方法获取根容器。