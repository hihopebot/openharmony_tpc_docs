
# ColorStateList
* openharmony API: ohos.agp.components.element.StateElement
* openharmony SDK版本：2.1.1.21
* IDE版本：2.1.0.501
* 实现方案:


原三方库：

对TextView设置ColorStateList使其在Normal、Pressed、Focused、Unable四种状态下显示不同的颜色
``` java
     /** 对TextView设置不同状态时其文字颜色。 */  
     private ColorStateList createColorStateList(int normal, int pressed, int focused, int unable) {  
         int[] colors = new int[] { pressed, focused, normal, focused, unable, normal };  
         int[][] states = new int[6][];  
         states[0] = new int[] { android.R.attr.state_pressed, android.R.attr.state_enabled };  
         states[1] = new int[] { android.R.attr.state_enabled, android.R.attr.state_focused };  
         states[2] = new int[] { android.R.attr.state_enabled };  
         states[3] = new int[] { android.R.attr.state_focused };  
         states[4] = new int[] { android.R.attr.state_window_focused };  
         states[5] = new int[] {};  
         ColorStateList colorList = new ColorStateList(states, colors);  
         return colorList;  
```

openharmony：

需要修改component组件交互效果/背景颜色
``` java
    public Text(Context context, AttrSet attrSet, String styleName) {
         super(context, attrSet, styleName);
         //通过属性值方式获取
         StateElement backgroudSelector = (StateElement)attrSet.getAttr(styleName).get().getElement();
 
         //或者通过以下方式获取
         StateElement backgroudSelector1 = (StateElement)this.getBackgroundElement();
 
         Element stateElement1 = backgroudSelector.getStateElement(0);
         int[] states1 = backgroudSelector.getStateSet(0);
 
         Element stateElement2 = backgroudSelector.getStateElement(1);
         int[] states2 = backgroudSelector.getStateSet(1);
 
         StateElement stateElement = new StateElement();
         stateElement.addState(states1,stateElement1);
         stateElement.addState(states2,stateElement2);
     }
```