﻿
# obtainStyledAttributes
* openharmony API:ohos.agp.components.AttrSet
* openharmony SDK版本：2.1.0.17
* IDE版本：2.1.0.141
* 实现方案:
``` java
  int mCustomLength = 0;
  if(attrSet.getAttr(customXmlName).isPresent()){
   mCustomLength = attrSet.getAttr(customXmlName).get().getDimensionValue();
  }
```