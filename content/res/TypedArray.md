﻿# TypedArray
* openharmony API: ohos.agp.components.AttrSet
* openharmony SDK版本：2.0.1.93及以上
* IDE版本：2.0.12.131
* 实现方案：自定义Component时添加自定义属性
##  新建一个java文件，配置需要的属性key
```java
public class AttrSetString {
    /** 幅值的大小，默认为100dp */
    public static String SIDEBAR_A = "SideBar_a";

    /** 文本滚动时字体大小，defaultalut :1 */
    public static String SIDEBAR_FONTSCALE = "SideBar_fontScale";

    /** 大文本大小，默认是文本大小的3倍 */
    public static String SIDEBAR_BIGTEXTSIZE = "SideBar_bigTextSize";

    /** BigText到峰值的距离，默认50dp */
    public static String SIDEBAR_GAPBETWEENTEXT = "SideBar_gapBetweenText";

    /** 抛物线开度大小，defaultalut 13 */
    public static String SIDEBAR_OPENCOUNT = "SideBar_openCount";
}
 ```
## 在xml中设置参数
```java
    <com.allenliu.sidebar.SideBar
            ohos:id="$+id:bar"
            ohos:align_parent_right="true"
            ohos:width="300vp"
            ohos:height="match_parent"
            ohos:right_padding="10vp"
            ohos:text_color="#00ff00"
            ohos:SideBar_a="300"
            ohos:SideBar_fontScale="2"
            ohos:text_size="12fp" />
 ```
##  在自定义控件中使用
```java
public class SideBar extends Text {
   private float amplitude = dp(100);
   private float fontScale = 1;
   private int gapBetweenText = dp(50);
   private int openCount = 13;
   private float bigTextSize;
    
    public SideBar(Context context, AttrSet attrs) {
       super(context, attrs);
       mContext = context;
       init(attrs);
    }
    
    private void init(AttrSet attrs) {
      amplitude = attrs.getAttr(SIDEBAR_A).isPresent() ? attrs.getAttr(SIDEBAR_A).get().getFloatValue() : 100.0f;
      fontScale = attrs.getAttr(SIDEBAR_FONTSCALE).isPresent() ? attrs.getAttr(SIDEBAR_FONTSCALE).get().getFloatValue() : 1;
      bigTextSize = attrs.getAttr(SIDEBAR_BIGTEXTSIZE).isPresent() ? attrs.getAttr(SIDEBAR_BIGTEXTSIZE).get().getFloatValue() : getTextSize() * 3;
      gapBetweenText = attrs.getAttr(SIDEBAR_GAPBETWEENTEXT).isPresent() ? attrs.getAttr(SIDEBAR_GAPBETWEENTEXT).get().getIntegerValue() : dp(50);
      openCount = attrs.getAttr(SIDEBAR_OPENCOUNT).isPresent() ? attrs.getAttr(SIDEBAR_OPENCOUNT).get().getIntegerValue() : 13;
    }

}
 
    
    
 ```