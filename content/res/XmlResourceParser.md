﻿### **getAttributeResourceValue**
>+ openharmony API: ohos.javax.xml.stream.XMLStreamReader.getAttributeValue
>+ openharmony SDK版本：2.1.0.17 以上
>+ IDE版本：2.1.0.141
>+ 实现方案：由于ohos.javax.xml.stream.XMLInputFactory只能解析xml得到基础的文本string，所以如果xml文件中的value使用的是型如$media:icon的引用型时，需要进行特殊的处理。

android assets/xml
```xml
<tabs>
    <tab
        icon="@drawable/ic_favorites"
        title="Favorites" 
         selectedColor="@color/myred"/>
</tabs>
```
harmony rawfile/xml
```xml
<tabs>
    <tab
        icon="$media:i0"
        title="Icon0"
         selectedColor="$color:myred"/>
</tabs>
```
此时通过XMLStreamReader.getAttributeValue能得到的是一个String值"$media:i0"，而我们需要的是icon图标的resId，此时我们可以通过icon的名字去反射entry/ResourceTable中的icon的resId，代码如下：
```java
   /**
     * invoke the resId of app bundle
     *
     * @param context      for get  package name
     * @param referenceStr for example {$media:i0_selected}
     * @return resource id
     */
    public static int invokeResId(Context context, String referenceStr) {
        String[] split = referenceStr.split(":");
        String type = split[0].substring(1);
        String fieldName = type.substring(0, 1).toUpperCase(Locale.ROOT) + type.substring(1) + "_" + split[1];
        try {
            Class<?> aClass = Class.forName(context.getBundleName() + ".ResourceTable");
            Field field = aClass.getDeclaredField(fieldName);
            return field.getInt(null);
        } catch (Exception e) {
            logger.log(Level.INFO, e.getMessage());
        }
        return -1;
    }
```
其他引用类型的value如"$color:myred"同样可以通过以上方式获取到resId进而拿到myred具体的颜色值

### **getAttributeBooleanValue**
>+ openharmony API: ohos.javax.xml.stream.XMLStreamReader.getAttributeValue
>+ openharmony SDK版本：2.1.0.17 以上
>+ IDE版本：2.1.0.141
>+ 实现方案：
```java
Boolean.parseBoolean(xMLStreamReader.getAttributeValue(i));
```