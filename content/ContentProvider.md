### **insert**
>+ openharmony API: ohos.aafwk.ability.DataAbilityHelper.insert
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
DataAbilityHelper helper = DataAbilityHelper.creator(this);

// 构造插入数据
ValuesBucket valuesBucket = new ValuesBucket();
valuesBucket.putString("name", "zhangsan");
valuesBucket.putInteger("age", 18);
valuesBucket.putFloat("height", 172.0f);
helper.insert(uri, valuesBucket);

```

### **query**
>+ openharmony API: ohos.aafwk.ability.DataAbilityHelper.query
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
DataAbilityHelper helper = DataAbilityHelper.creator(this);

// 构造查询条件
DataAbilityPredicates predicates = new DataAbilityPredicates();
predicates.between("id", 1, 10);

// 进行查询
ResultSet resultSet = helper.query(uri, columns, predicates);

```

### **update**
>+ openharmony API: ohos.aafwk.ability.DataAbilityHelper.update
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
DataAbilityHelper helper = DataAbilityHelper.creator(this);

// 构造更新条件
DataAbilityPredicates predicates = new DataAbilityPredicates();
predicates.equalTo("id", 1);

// 构造更新数据
ValuesBucket valuesBucket = new ValuesBucket();
valuesBucket.putString("name", "zhangsan");
valuesBucket.putInteger("age", 18);
valuesBucket.putFloat("height", 172.0f);
helper.update(uri, valuesBucket, predicates);

```

### **delete**
>+ openharmony API: ohos.aafwk.ability.DataAbilityHelper.delete
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
DataAbilityHelper helper = DataAbilityHelper.creator(this);

// 构造删除条件
DataAbilityPredicates predicates = new DataAbilityPredicates();
predicates.between("id", 1, 10);
helper.delete(uri, predicates);

```

### **bulkInsert**
>+ openharmony API: ohos.aafwk.ability.DataAbilityHelper.batchInsert
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
DataAbilityHelper helper = DataAbilityHelper.creator(this);

// 构造插入数据
ValuesBucket[] values = new ValuesBucket[2];
values[0] = new ValuesBucket();
values[0].putString("name", "zhangsan");
values[0].putInteger("age", 18);
values[1] = new ValuesBucket();
values[1].putString("name", "lisi");
values[1].putInteger("age", 20);
helper.batchInsert(uri, values);

```

### **applyBatch**
>+ openharmony API: ohos.aafwk.ability.DataAbilityHelper.executeBatch
>+ openharmony SDK版本：2.1.1.21 以上
>+ IDE版本：2.1.0.501
>+ 实现方案：替换使用,使用方法如下:
```java
DataAbilityHelper helper = DataAbilityHelper.creator(context, insertUri);

// 构造批量操作
ValuesBucket value1 = new ValuesBucket();
DataAbilityOperation opt1 = DataAbilityOperation.newInsertBuilder(insertUri).withValuesBucket(value1).build();
ValuesBucket value2 = new ValuesBucket();
DataAbilityOperation opt2 = DataAbilityOperation.newInsertBuilder(insertUri).withValuesBucket(value2).build();
List<DataAbilityOperation> operations = new ArrayList<>();
operations.add(opt1);
operations.add(opt2);
DataAbilityResult[] result = helper.executeBatch(insertUri, operations);        

```
