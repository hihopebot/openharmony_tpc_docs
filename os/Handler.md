﻿## handleMessage(Message msg)
>+ openharmony API:ohos.eventhandler.EventHandler
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案：新建类继承EventHandler，重写processEvent方法
``` java
public class MyHandler extends EventHandler {

 @Override
    protected void processEvent(InnerEvent event) {
        if(MSG_ID==event.eventId){
            //dosomething
        }
    }
}
```