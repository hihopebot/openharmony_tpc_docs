### **prepare**
>+  openharmony API:ohos.eventhandler.EventRunner.create
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：直接替换

### **prepareMainLooper**
>+  openharmony API:ohos.eventhandler.EventRunner.create
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：直接替换

### **myLooper**
>+  openharmony API:ohos.eventhandler.EventRunner.current
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：直接替换

### **isCurrentThread**
>+  openharmony API:ohos.eventhandler.EventRunner.isCurrentRunnerThread
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：直接替换

### **getMainLooper**
>+  openharmony API:ohos.eventhandler.EventRunner.getMainEventRunner
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：直接替换

### **loop**
>+  openharmony API:ohos.eventhandler.EventRunner.run
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：直接替换

### **quit**
>+  openharmony API:ohos.eventhandler.EventRunner.stop
>+  openharmony SDK版本：2.1.1.21
>+  IDE版本：2.1.0.501
>+  实现方案：直接替换

