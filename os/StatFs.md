﻿
### **StatFs**
>+ openharmony class：`ohos.data.usage.StatVfs`
>+ openharmony SDK版本：2.1.1.21
>+ IDE版本：2.1.0.501
>+ 实现方案：查询外置存储器的剩余空间

* 原库实现
```java
	File sdcardDir = Environment.getExternalStorageDirectory();        
	StatFs sf = new StatFs(sdcardDir.getPath());     	
	return sf.getAvailableBlocks() * sf.getBlockSize();
```

* 替换实现
```java
	StatVfs statVfs = new StatVfs(context.getExternalCacheDir().getAbsolutePath());
	return statVfs.getAvailableSpace();
```