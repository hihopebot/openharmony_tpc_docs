﻿### **elapsedRealtime**
>+  openharmony API:ohos.miscservices.timeutility.Time.getRealTime
>+  openharmony SDK版本：2.1.0.17
>+  IDE版本：2.1.0.141
>+  实现方案 使用 getRealTime()方法

```java
long time = Time.getRealTime();
```

### **currentThreadTimeMillis**
>+ openharmony API:ohos.miscservices.timeutility.Time.getCurrentThreadTime
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案： 使用 getCurrentThreadTime()方法
```java
long time = Time.getCurrentThreadTime();
```


### **setCurrentTimeMillis**
>+ openharmony API:ohos.miscservices.timeutility.Time.setTime
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案： 使用 setTime(Context context,Date date)方法

```java
long time = Time.setTime(context , date);
```

### **sleep**
>+ openharmony API:ohos.miscservices.timeutility.Time.sleep
>+ openharmony SDK版本：2.1.0.17
>+ IDE版本：2.1.0.141
>+ 实现方案： 使用 sleep(Long ms)方法

```java
Time.sleep(milliSecond);
```


      